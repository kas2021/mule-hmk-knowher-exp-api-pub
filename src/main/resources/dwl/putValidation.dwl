%dw 2.0
import * from dw::core::Strings

output application/json
---
[] + 

	if (attributes.headers['client'] == null)
// 400       
      		{
				ErrorMessage : Mule::p('putConsumer.missingClientHeader.ErrorMessage'),        		
				ErrorCode : Mule::p('putConsumer.missingClientHeader.ErrorCode')
      		}
	else    
    if (!(Mule::p('valid.client') contains upper(attributes.headers['client'])))
// 400 
   		    {
			    ErrorMessage: Mule::p('putConsumer.invalidClient.ErrorMessage'),
			    ErrorCode: Mule::p('putConsumer.invalidClient.ErrorCode')  					    
			}   	
	else 		 
    if (!isEmpty(payload.DateOfBirth)  
    	and ((((payload.DateOfBirth[0 to 1] default 0 as Number) < 1) or ((payload.DateOfBirth[0 to 1] default 0 as Number) > 12))
    	or (((payload.DateOfBirth[2 to 3] default 0 as Number) < 1) or ((payload.DateOfBirth[2 to 3] default 0 as Number) > 31)))
    )
// 1002    		
    		{
				ErrorMessage : Mule::p('putConsumer.birthDateInvalid.ErrorMessage'),		    	
				ErrorCode : Mule::p('putConsumer.birthDateInvalid.ErrorCode')
			}
	else
	if (payload.FirstName != null and (sizeOf(payload.FirstName)>15))
// 1003 			
			{
				ErrorMessage : Mule::p('putConsumer.firstNameLength.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.firstNameLength.ErrorCode')
			}	
	else
	if (payload.LastName != null  and sizeOf(payload.LastName)<2 )
// 1004			
			{
				ErrorMessage : Mule::p('putConsumer.lastNameMinLength.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.lastNameMinLength.ErrorCode')
			}	
	else 
	if (payload.LastName != null  and (sizeOf(payload.LastName)>20))
// 1005			
			{
				ErrorMessage : Mule::p('putConsumer.lastNameMaxLength.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.lastNameMaxLength.ErrorCode')
			}	
	else 
	if (payload.Email != null  and 
		(sizeOf(payload.Email default "") < 6 
			or sizeOf(payload.Email default "") > 50
			or !(contains(payload.Email default "","@"))
			or !(contains(payload.Email default "","."))
		))
// 1008				
			{
				ErrorMessage : Mule::p('putConsumer.emailInvalidFormat.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.emailInvalidFormat.ErrorCode')
			}
	else
   	if (!isBlank(payload.HomePhone) and (!(sizeOf(payload.HomePhone as String default "") == 10)))
// 1009			
			{
				ErrorMessage : Mule::p('putConsumer.homePhoneLength.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.homePhoneLength.ErrorCode')
			} 
	else 
//	if (payload.HomePhone != null and (!(isNumeric(payload.HomePhone default 0))))
// 1010
// 		    {
//				ErrorMessage : Mule::p('putConsumer.homePhoneNumericOnly.ErrorMessage'),				
//				ErrorCode : Mule::p('putConsumer.homePhoneNumericOnly.ErrorCode')				
//			} 
	//else 
//	if ??
// 1011 
// 		    {
//				ErrorMessage : Mule::p('putConsumer.crownRewardsLength.ErrorMessage'),				
//				ErrorCode : Mule::p('putConsumer.crownRewardsLength.ErrorCode')				
//			} 
//	else 
//	if ??
// 1012
// 		    {
//				ErrorMessage : Mule::p('putConsumer.crownRewardsNumericOnly.ErrorMessage'),				
//				ErrorCode : Mule::p('putConsumer.crownRewardsNumericOnly.ErrorCode')				
//			} 
//	else 
//	if ?? 
// 1013 
// 		    {
//				ErrorMessage : Mule::p('putConsumer.kocValidation.ErrorMessage'),				
//				ErrorCode : Mule::p('putConsumer.kocValidation.ErrorCode')				
//			} 
//	else 
    if (payload.State != null  and !(Mule::p('valid.states.US') contains upper(payload.State default "")))
// 1014				
			{
				ErrorMessage : Mule::p('putConsumer.stateCodeInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.stateCodeInvalid.ErrorCode')

			}
	else
	if (payload.Gender != null  and !(Mule::p('valid.gender') contains upper(payload.Gender)))
// 1015			
			{
				ErrorMessage : Mule::p('putConsumer.genderType.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.genderType.ErrorCode')
			} 
			
	else 
	if (payload.MiddleInitial != null  
		and !(sizeOf(payload.MiddleInitial default "") == 1 )
		and (payload.MiddleInitial matches (/[A-Z]/))
	)
// 1016			
			{
				ErrorMessage : Mule::p('putConsumer.middleInitial.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.middleInitial.ErrorCode')
			} 

	else
	if (payload.AddressLine1 != null  and sizeOf(payload.AddressLine1 default "")>50)
// 1017 			
			{
				ErrorMessage : Mule::p('putConsumer.addressLine1Length.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.addressLine1Length.ErrorCode')
			}	
	else
	if (payload.AddressLine2 != null  and sizeOf(payload.AddressLine2 default "")>35)
// 1018 			
			{
				ErrorMessage : Mule::p('putConsumer.addressLine2Length.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.addressLine2Length.ErrorCode')
			}	
	else
	if (payload.City != null  and sizeOf(payload.City default "")>31)
// 1019 			
			{
				ErrorMessage : Mule::p('putConsumer.cityLength.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.cityLength.ErrorCode')
			}	
	else
	if (payload.Country != null  and !(Mule::p('valid.country') contains upper(payload.Country default "")))
// 1020 			
			{
				ErrorMessage : Mule::p('putConsumer.countryCode.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.countryCode.ErrorCode')
			}	
	else
    if (payload.AddressLine1 != null  and (payload.City == null or isEmpty(payload.City)))
// 2002			
			{
				ErrorMessage : Mule::p('putConsumer.addressMissingCity.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.addressMissingCity.ErrorCode')

			}
	else
    if (payload.AddressLine1 != null  and (payload.State == null or isEmpty(payload.State)))
// 2002			
			{
				ErrorMessage : Mule::p('putConsumer.addressMissingState.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.addressMissingState.ErrorCode')

			}
	else
    if (payload.AddressLine1 != null  and (payload.Country == null or isEmpty(payload.Country)))
// 2002			
			{
				ErrorMessage : Mule::p('putConsumer.addressMissingCountry.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.addressMissingCountry.ErrorCode')

			}
	else
    if (payload.AddressLine1 != null  and (payload.Zip == null or isEmpty(payload.Zip)))
// 2002			
			{
				ErrorMessage : Mule::p('putConsumer.addressMissingZip.ErrorMessage'),				
				ErrorCode : Mule::p('putConsumer.addressMissingZip.ErrorCode')

			}
    else
    if (payload.Zip != null and (!((sizeOf(payload.Zip default "") == 5) or (sizeOf(payload.Zip default "") == 9))))
// 1006
		    {
				ErrorMessage : Mule::p('putConsumer.zipCodeLength.ErrorMessage'),		  		
				ErrorCode : Mule::p('putConsumer.zipCodeLength.ErrorCode')				
			} 
	else 
    if (payload.Zip != null and !isNumeric(payload.Zip default ""))
// 1007
		    {
				ErrorMessage : Mule::p('putConsumer.zipCodeNumericOnly.ErrorMessage'),		  		
				ErrorCode : Mule::p('putConsumer.zipCodeNumericOnly.ErrorCode')				
			} 
	else	
    if (payload.AddKocMembership != null  and (payload.KocNumber == null or isEmpty(payload.KocNumber) ))
// 2005				
			{
				ErrorMessage : Mule::p('postConsumer.kocMemberRequired.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.kocMemberRequired.ErrorCode')

			}
	else

			{
			    ErrorMessage : 'Ok',				
			    ErrorCode : '200'

			}
