%dw 2.0
output application/json

fun cemsTS(khTS) = khTS as DateTime as String{format:"M/dd/YYYY h:mm:ss a"}   
fun cemsDate(khTS) = khTS as DateTime as String {format:'M/d/yyyy'} 

fun convToBoolean(opts) = if (opts == 'Y' or opts == 'IN') true else false                 
---

{
     "Errors": [],
     "Result": 
         (vars.currentKHData map (item, index) ->
             {
                "ConsumerId": item.sourceSystemId as String,
                "Memberships": if (sizeOf(item.membership.memberType default "") > 0) ((item.membership filter ($.status != "PURGED")) map (mbr,index2) ->
                 {
                        "membershipType": if (mbr.memberType contains 'CROWN') 'Crown Rewards' else if (mbr.memberType contains 'KEEPSAKE') 'Keepsake Ornaments Club' else "",
                        "id": mbr.memberId as Number,
                        "memberCurrentStatus":  
                        	(if (mbr.status == 'EARN AND ISSUE') '11' 
                        	else if (mbr.status == 'ENROLLED' ) '31' 
                        	else if (mbr.status == 'EXPIRED' ) '44' 
                        	else if (mbr.status == 'ELIGIBLE') '11' 
                        	else if (mbr.status == 'CANCELLED' or mbr.status == 'MERGED MEMBERSHIP') '42' 
                        	else if (mbr.status == 'NO EARN AND NO ISSUE' or mbr.status == 'TERMINATED') '45' 
                        	else if (mbr.status == 'INELIGIBLE') '12'  
                        	else if (mbr.status == 'EXPELLED') '43' 
                        	else 0) as String,
                        "memberCurrentStatusHD": mbr.status,
                        "memberLastUpdate": cemsTS(mbr.lastUpdateUTC),
                        "CrownRewardsLoyalty": {
                            "currentTierNumber": (if (mbr.memberType contains 'CROWN') mbr.currentTier else 0) as Number,
                            "MemberPointsAvailable": (if (mbr.memberType contains 'CROWN') mbr.currentPoints else 0) as Number,
                            "currentTier": if (mbr.memberType contains 'CROWN') 
                            					(if (mbr.currentTierDescription == 'Crown Rewards') 'PLATINUM' 
                            						else if (mbr.currentTierDescription == 'Platinum') 'CONGRATS ON PLATINUM'
                            						else null
                            					) else null, 
                            "CardCount": 0 // Default since Aimia data is stale that CEMS pulls
                        },
                        "KeepsakeOrnamentClub": {
                            "CharterMember": if ((mbr.memberType contains 'KEEPSAKE') and (mbr.tierLevel == "Y")) true else false,
                            "MemberSince": if ((mbr.memberType contains 'KEEPSAKE') and mbr.memberSinceYear !=null)  mbr.memberSinceYear[0 to 3] else 0,
                            "DateExpires": if ((mbr.memberType contains 'KEEPSAKE') and mbr.memberExpiry != null) ((mbr.memberExpiry as DateTime) as String{format:"YYYYMM"}) else null
                        }
                    } ) else []

             })[0]
         
     
}


