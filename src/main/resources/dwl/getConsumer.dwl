%dw 2.0
output application/json

fun latestAddr(ix) = (((payload[ix].address map ($)) 
                    orderBy $.lastUpdateUTC)
                    orderBy -$$)[0]

fun latestHomePhone(ix) = (((payload[ix].phone filter($.phoneType == 'HOME') map ($)) 
                    orderBy $.lastUpdateUTC)
                    orderBy -$$)[0]

fun latestMobilePhone(ix) = (((payload[ix].phone filter($.phoneType == 'MOBILE') map ($)) 
                    orderBy $.lastUpdateUTC)
                    orderBy -$$)[0]

fun latestEmail(ix) = (((payload[ix].email map ($)) 
					orderBy $.lastUpdateUTC) 
					orderBy -$$)[0]
					
fun latestAltId(ix) = (((payload[ix].alternateId filter($.typ == 'HDXREF') map ($)) 
					orderBy $.lastUpdateUTC) 
					orderBy -$$)[0]
										
fun cemsTS(khTS) = khTS as DateTime as String{format:"M/dd/YYYY h:mm:ss a"}   
fun cemsDate(khTS) = khTS as DateTime as String {format:'M/d/yyyy'} 

fun convToBoolean(opts) = if (opts == 'Y' or opts == 'IN') true else false                 
---

{
     "Errors": [],
     "Result": {
         "numberOfResults": sizeOf(payload),
         "consumers":  payload map (item, index) ->
             {
                "consumerId": item.sourceSystemId as Number,
                "alternateId":	
                	{
                		val:	latestAltId(index).val,
                		typ:	latestAltId(index).typ
                	},                
                "name":    
                    {
                        "firstName": item.firstName,
                        "middleInitial": item.middleInitial default "",
                        "lastName": item.lastName
                    },
                "address": 
                    {
                        "addressLine1": latestAddr(index).addressLine1,
                        "addressLine2": latestAddr(index).addressLine2 default "",
                        "city": latestAddr(index).city,
                        "state": latestAddr(index).state,
                        "zip": latestAddr(index).postalCode ++ latestAddr(index).postalCodeExt default "",
                        "country": if (latestAddr(index).country == 'US')  'USA' else latestAddr(index).country,
                        "deliverable": true, // How do we know if address deliverable? 
                        "optIns": 
                            {
                                "postalOptIn": convToBoolean(latestAddr(index).addressOptIn.optIn), 
                                "subsidiaryMailOptIn": convToBoolean(""), // This is absent in MDM,  
                                "thirdPartyMailOptIn": convToBoolean("") // Again absent in MDM 
                            }                     
                    },
                "phone": 
                    {
                        "homeNumber": (latestHomePhone(index).phone default 0) as Number,
                        "mobileNumber": (latestMobilePhone(index).phone default 0) as Number,
                        "mobileCaptureDate": if (latestMobilePhone(index).lastUpdateUTC != null) cemsDate(latestMobilePhone(index).lastUpdateUTC) else null,
                        "optIns": 
                            {
                                "phoneOptIn": convToBoolean(latestHomePhone(index).phoneOptIn.optIn),
                                "smsOptIn": Mule::lookup('sms-status-lookup-Flow', (latestMobilePhone(index).phone default 0) as Number ) default false,
                                "textOptIn": convToBoolean(""), // Different than above??
                                "smsOptCaptureDate": if (latestMobilePhone(index).phoneOptIn.lastUpdateUTC != null) cemsDate(latestMobilePhone(index).phoneOptIn.lastUpdateUTC) else null
                            }
                    },
                "email": if (isEmpty(latestEmail(index))) null 
                else
                	{
                		"address":	latestEmail(index).email,
                	    "captureDate": if (latestEmail(index).lastUpdateUTC != null) cemsDate(latestEmail(index).lastUpdateUTC) else null,
                    	"lastCaptureDate": if (latestEmail(index).lastUpdateUTC != null) cemsDate(latestEmail(index).lastUpdateUTC) else null,
                    	"deliverable": true,
                    	"optIns": 
                    		{
                        		"optIn": convToBoolean(latestEmail(index).emailOptIn.optIn) ,
                        		"subsidiaryOptIn": true 
                    		}
                		},
                "dateOfBirth": if (item.birthDate != null) (item.birthDate as Date as String {format:"MMddyyyy"}) else "",
                "gender": item.gender default "",
                "deceased": if (item.aliveInd == false) true else false,
                "hallmarkSweepstakesOpt": true, //?? 
                "lastUpdate": cemsTS(item.lastUpdateUTC),
                "memberships": if (sizeOf(item.membership.memberType default "") > 0) ((item.membership filter ($.status != "PURGED")) map (mbr,index2) ->
                 {
                        "membershipType": if (mbr.memberType contains 'CROWN') 'Crown Rewards' else if (mbr.memberType contains 'KEEPSAKE') 'Keepsake Ornaments Club' else "",
                        "id": mbr.memberId as Number,
                        "memberCurrentStatus":  
                        	(if (mbr.status == 'EARN AND ISSUE') '11' 
                        	else if (mbr.status == 'ENROLLED' ) '31' 
                        	else if (mbr.status == 'EXPIRED' ) '44' 
                        	else if (mbr.status == 'ELIGIBLE') '11' 
                        	else if (mbr.status == 'CANCELLED' or mbr.status == 'MERGED MEMBERSHIP') '42' 
                        	else if (mbr.status == 'NO EARN AND NO ISSUE' or mbr.status == 'TERMINATED') '45' 
                        	else if (mbr.status == 'INELIGIBLE') '12'  
                        	else if (mbr.status == 'EXPELLED') '43' 
                        	else 0) as String,
                        "memberCurrentStatusHD": mbr.status,
                        "memberLastUpdate": cemsTS(mbr.lastUpdateUTC),
                        "CrownRewardsLoyalty": {
                            "currentTierNumber": (if (mbr.memberType contains 'CROWN') mbr.currentTier else 0) as Number,
                            "MemberPointsAvailable": (if (mbr.memberType contains 'CROWN') mbr.currentPoints else 0) as Number,
                            "currentTier": if (mbr.memberType contains 'CROWN') 
                            					(if (mbr.currentTierDescription == 'Crown Rewards') 'PLATINUM' 
                            						else if (mbr.currentTierDescription == 'Platinum') 'CONGRATS ON PLATINUM'
                            						else null
                            					) else null, 
                            "CardCount": 0 // Default since Aimia data is stale that CEMS pulls
                        },
                        "KeepsakeOrnamentClub": {
                            "CharterMember": if ((mbr.memberType contains 'KEEPSAKE') and (mbr.tierLevel == "Y")) true else false,
                            "MemberSince": if ((mbr.memberType contains 'KEEPSAKE') and mbr.memberSinceYear !=null)  mbr.memberSinceYear[0 to 3] else 0,
                            "DateExpires": if ((mbr.memberType contains 'KEEPSAKE') and mbr.memberExpiry != null) ((mbr.memberExpiry as DateTime) as String{format:"yyyyMM"}) else null
                        }
                    } ) else []

             }
         
     }
     
}


