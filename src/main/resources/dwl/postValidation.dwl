%dw 2.0
import * from dw::core::Strings

output application/json
---
[] + 

	if (attributes.headers['client'] == null)
// 400       
      		{
				ErrorMessage : Mule::p('postConsumer.missingClientHeader.ErrorMessage'),        		
				ErrorCode : Mule::p('postConsumer.missingClientHeader.ErrorCode')
      		}
	else    
    if (!(Mule::p('valid.client') contains upper(attributes.headers['client'])))
// 400 
   		    {
			    ErrorMessage: Mule::p('postConsumer.invalidClient.ErrorMessage'),
			    ErrorCode: Mule::p('postConsumer.invalidClient.ErrorCode')  					    
			}   
	else 		 
	if (payload.MiddleInitial != null  and (sizeOf(payload.MiddleInitial default "") > 1))
// 1005			
			{
				ErrorMessage : Mule::p('postConsumer.middleInitial.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.middleInitial.ErrorCode')
			} 
			
	else 
    if (payload.Zip != null and (!((sizeOf(payload.Zip default "") == 5) or (sizeOf(payload.Zip default "") == 9))))
// 1006
		    {
				ErrorMessage : Mule::p('postConsumer.zipCodeLength.ErrorMessage'),		  		
				ErrorCode : Mule::p('postConsumer.zipCodeLength.ErrorCode')				
			} 
	else 
    if (payload.Zip != null and !isNumeric(payload.Zip default ""))
// 1006
		    {
				ErrorMessage : Mule::p('postConsumer.zipCodeNumericOnly.ErrorMessage'),		  		
				ErrorCode : Mule::p('postConsumer.zipCodeNumericOnly.ErrorCode')				
			} 
	else			
   	if (payload.HomePhone != null and (!(sizeOf(payload.HomePhone as String default "") == 10)))
// 1007			
			{
				ErrorMessage : Mule::p('postConsumer.homePhoneSize.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.homePhoneSize.ErrorCode')
			} 
	else 
//	if (payload.HomePhone != null and (!(isNumeric(payload.HomePhone default ""))))
// 1007
// 		    {
//				ErrorMessage : Mule::p('postConsumer.homePhoneNumericOnly.ErrorMessage'),				
//				ErrorCode : Mule::p('postConsumer.homePhoneNumericOnly.ErrorCode')				
//			} 
//	else 
   	if (payload.WorkPhone != null and (!(sizeOf(payload.WorkPhone default "") == 10) or (sizeOf(payload.WorkPhone default "") == 14)))
// 1007			
			{
				ErrorMessage : Mule::p('postConsumer.workPhoneSize.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.workPhoneSize.ErrorCode')
			} 
	else 
	if (payload.FirstName != null and (sizeOf(payload.FirstName ) == 0))
// 1008			
			{
				ErrorMessage : Mule::p('postConsumer.firstNameEmpty.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.firstNameEmpty.ErrorCode')
			}	
	else 
	if (payload.LastName != null  and (sizeOf(payload.LastName)<2 or sizeOf(payload.LastName)>20))
// 1008			
			{
				ErrorMessage : Mule::p('postConsumer.lastNameLength.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.lastNameLength.ErrorCode')
			}	
	else 
	if (payload.LastName != null  and sizeOf(payload.LastName) == 0)
// 1009		
			{
				ErrorMessage : Mule::p('postConsumer.lastNameEmpty.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.lastNameEmpty.ErrorCode')
			}	
	else 
	if (payload.AddressLine1 != null  and sizeOf(payload.AddressLine1 default "")>50)
// 1010 			
			{
				ErrorMessage : Mule::p('postConsumer.addressLine1Length.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.addressLine1Length.ErrorCode')
			}	
	else
	if (payload.AddressLine2 != null  and sizeOf(payload.AddressLine2 default "")>35)
// 1010 			
			{
				ErrorMessage : Mule::p('postConsumer.addressLine2Length.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.addressLine2Length.ErrorCode')
			}	
	else
	if (payload.City != null  and sizeOf(payload.City default "")>31)
// 1010 			
			{
				ErrorMessage : Mule::p('postConsumer.cityLength.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.cityLength.ErrorCode')
			}	
	else
	if (payload.DateOfBirth != null  and !(payload.DateOfBirth default "" matches(/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/ )))
// 1010    	
    		{
				ErrorMessage : Mule::p('postConsumer.invalidDate.ErrorMessage'),		    	
				ErrorCode : Mule::p('postConsumer.invalidDate.ErrorCode')
			}
	else
    if (payload.DateOfBirth != null  and ((payload.DateOfBirth[0 to 1] default 0 as Number) < 1) or ((payload.DateOfBirth[0 to 1] default 0 as Number) > 12))
// 1010    		
    		{
				ErrorMessage : Mule::p('postConsumer.birthDateMonthError.ErrorMessage'),		    	
				ErrorCode : Mule::p('postConsumer.birthDateMonthError.ErrorCode')
			}
			
	else
if (payload.DateOfBirth != null  
    	and ((((payload.DateOfBirth[0 to 1] default 0 as Number) < 1) or ((payload.DateOfBirth[0 to 1] default 0 as Number) > 12))
    	or (((payload.DateOfBirth[2 to 3] default 0 as Number) < 1) or ((payload.DateOfBirth[2 to 3] default 0 as Number) > 31)))
    )
// 1002    		
    		{
				ErrorMessage : Mule::p('postConsumer.birthDateInvalid.ErrorMessage'),		    	
				ErrorCode : Mule::p('postConsumer.birthDateInvalid.ErrorCode')
			}
 	else	
	if (payload.Email != null  and (sizeOf(payload.Email default "")<6 or sizeOf(payload.Email default "")> 50))
// 1012				
			{
				ErrorMessage : Mule::p('postConsumer.emailInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.emailInvalid.ErrorCode')
			}
	else
	if (payload.Email != null  and !(contains(payload.Email default "","@")))
// 1013				
			{
				ErrorMessage : Mule::p('postConsumer.emailInvalid2.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.emailInvalid2.ErrorCode')

			}
	else
	if (payload.Email != null  and !(contains(payload.Email default "",".")))
// 1014				
			{
				ErrorMessage : Mule::p('postConsumer.emailInvalid3.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.emailInvalid3.ErrorCode')

			}
	else
	if (payload.Gender != null  and !(Mule::p('valid.gender') contains upper(payload.Gender)))
// 1015			
			{
				ErrorMessage : Mule::p('postConsumer.genderType.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.genderType.ErrorCode')
			} 
	else 
    if (payload.State != null  and !(Mule::p('valid.states.US') contains upper(payload.State default "")))
// 2001				
			{
				ErrorMessage : Mule::p('postConsumer.stateCodeInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.stateCodeInvalid.ErrorCode')

			}
	else
    if (payload.AddressLine1 != null  and (payload.City == null or isEmpty(payload.City)))
// 2002			
			{
				ErrorMessage : Mule::p('postConsumer.addressMissingCity.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.addressMissingCity.ErrorCode')

			}
	else
    if (payload.AddressLine1 != null  and (payload.State == null or isEmpty(payload.State)))
// 2002			
			{
				ErrorMessage : Mule::p('postConsumer.addressMissingState.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.addressMissingState.ErrorCode')

			}
	else
    if (payload.AddressLine1 != null  and (payload.Country == null or isEmpty(payload.Country)))
// 2002			
			{
				ErrorMessage : Mule::p('postConsumer.addressMissingCountry.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.addressMissingCountry.ErrorCode')

			}
	else
    if (payload.AddressLine1 != null  and (payload.Zip == null or isEmpty(payload.Zip)))
// 2002			
			{
				ErrorMessage : Mule::p('postConsumer.addressMissingZip.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.addressMissingZip.ErrorCode')

			}
	else
    if (payload.KocNumber != null  and sizeOf(payload.KocNumber default "")>12)
// 2003				
			{
				ErrorMessage : Mule::p('postConsumer.kocMemberInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.kocMemberInvalid.ErrorCode')

			}
	else
    if (payload.VendorId != null  and sizeOf(payload.VendorId default "")>18)
// 2004				
			{
				ErrorMessage : Mule::p('postConsumer.vendorInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.vendorInvalid.ErrorCode')

			}
	else
    if (payload.AddKocMembership != null  and (payload.KocNumber == null or isEmpty(payload.KocNumber) ))
// 2005				
			{
				ErrorMessage : Mule::p('postConsumer.kocMemberRequired.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.kocMemberRequired.ErrorCode')

			}
	else
//    if (payload.country != null  and !(payload.country == 'US' or payload.country == 'USA'))
// 9015	hdRequired?? 			
//			{
//				ErrorMessage : Mule::p('postConsumer.crownRule.ErrorMessage'),				
//				ErrorCode : Mule::p('postConsumer.crownRule.ErrorCode')
//
//			}
//	else
    if (payload.Country != null  and !(Mule::p('valid.country') contains upper(payload.Country default "")))
// 9016				
			{
				ErrorMessage : Mule::p('postConsumer.crownRule.ErrorMessage'),				
				ErrorCode : Mule::p('postConsumer.crownRule.ErrorCode')

			}
	else
			{
			    ErrorMessage : 'Ok',				
			    ErrorCode : '200'

			}
