%dw 2.0
import * from dw::core::Strings

output application/json
---
[] + 

	if (message.attributes.headers['client'] == null)
// 400       
      		{
				ErrorMessage : Mule::p('getConsumer.missingClientHeader.ErrorMessage'),        		
				ErrorCode : Mule::p('getConsumer.missingClientHeader.ErrorCode')
      		}
	else    
	if (!(Mule::p('valid.client') contains upper(message.attributes.headers['client'])))
// 400       
      		{
				ErrorMessage : Mule::p('getConsumer.invalidClient.ErrorMessage'),        		
				ErrorCode : Mule::p('getConsumer.invalidClient.ErrorCode')
      		}
	else    
	if (message.attributes.queryParams.consumerId != null and sizeOf(message.attributes.queryParams.consumerId default "")>12)
// 400			
			{
				ErrorMessage : Mule::p('getConsumer.consumerIdInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.consumerIdInvalid.ErrorCode')
			}
			
	else 
    if (!(message.attributes.queryParams.homePhone != null 
    	or ( message.attributes.queryParams.lastName != null and message.attributes.queryParams.zip != null)
    	or message.attributes.queryParams.consumerId != null 
    	or message.attributes.queryParams.memberId != null
    	or message.attributes.queryParams.email != null
    	or message.attributes.queryParams.alternateId != null))
// 1001				
			{
				ErrorMessage : Mule::p('getConsumer.moreInfo.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.moreInfo.ErrorCode')

			}
	else
	if (message.attributes.queryParams.firstName != null and sizeOf(message.attributes.queryParams.firstName  default "")>15 )
// 1003 			
			{
				ErrorMessage : Mule::p('getConsumer.firstNameTooLong.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.firstNameTooLong.ErrorCode')
			}	
	else
    if (message.attributes.queryParams.zip != null and (!(message.attributes.queryParams.zip default "" matches(/[0-9]*/ ))))
// 1006
		    {
				ErrorMessage : Mule::p('getConsumer.zipInvalid.ErrorMessage'),		  		
				ErrorCode : Mule::p('getConsumer.zipInvalid.ErrorCode')				
			}
	else 
	if (message.attributes.queryParams.homePhone != null and !(isNumeric(message.attributes.queryParams.homePhone)))
// 1007
 		    {
				ErrorMessage : Mule::p('getConsumer.homePhoneInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.homePhoneInvalid.ErrorCode')				
			}
	else 
   	if (message.attributes.queryParams.homePhone != null and sizeOf(message.attributes.queryParams.homePhone default "") != 10)
// 1007			
			{
				ErrorMessage : Mule::p('getConsumer.homePhoneLength.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.homePhoneLength.ErrorCode')
			}
	else 
	if (message.attributes.queryParams.lastName != null and (sizeOf(message.attributes.queryParams.lastName  default "")<2 or sizeOf(message.attributes.queryParams.lastName  default "")>20))
// 1008			
			{
				ErrorMessage : Mule::p('getConsumer.lastNameLength.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.lastNameLength.ErrorCode')
			}	
	else 
	if (message.attributes.queryParams.dateOfBirth != null and !(message.attributes.queryParams.dateOfBirth default "" matches(/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/ )))
// 1010    	
    		{
				ErrorMessage : Mule::p('getConsumer.dateFormatError.ErrorMessage'),		    	
				ErrorCode : Mule::p('getConsumer.dateFormatError.ErrorCode')
			}
	else
    if (message.attributes.queryParams.dateOfBirth != null and 
    	((message.attributes.queryParams.dateOfBirth[0 to 1] as Number) < 1 or (message.attributes.queryParams.dateOfBirth[0 to 1] as Number > 12) or (message.attributes.queryParams.dateOfBirth[2 to 3] as Number < 1 ) or (message.attributes.queryParams.dateOfBirth[2 to 3] as Number > 31)))
// 1010    		
    		{
				ErrorMessage : Mule::p('getConsumer.birthDateMonthError.ErrorMessage'),		    	
				ErrorCode : Mule::p('getConsumer.birthDateMonthError.ErrorCode')
			}
			
	else
	if (message.attributes.queryParams.dateOfBirth != null and ((now() as Date) < message.attributes.queryParams.dateOfBirth as Date {format: 'MMddyyyy'}))
// 1010			
			{
				ErrorMessage : Mule::p('getConsumer.birthDateError.ErrorMessage'),		    	
				ErrorCode : Mule::p('getConsumer.birthDateError.ErrorCode')
			}
		
	else
	if (message.attributes.queryParams.email != null and (sizeOf(message.attributes.queryParams.email  default "")<6 or sizeOf(message.attributes.queryParams.email  default "")> 50))
// 1012				
			{
				ErrorMessage : Mule::p('getConsumer.emailInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.emailInvalid.ErrorCode')
			}
	else
	if (message.attributes.queryParams.email != null and !(contains(message.attributes.queryParams.email default "","@")))
// 1013				
			{
				ErrorMessage : Mule::p('getConsumer.emailFormatInvalid.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.emailFormatInvalid.ErrorCode')

			}
	else
	if (message.attributes.queryParams.email != null and !(contains(message.attributes.queryParams.email default "",".")))
// 1014				
			{
				ErrorMessage : Mule::p('getConsumer.emailFormatInvalid2.ErrorMessage'),				
				ErrorCode : Mule::p('getConsumer.emailFormatInvalid2.ErrorCode')

			}
	else
			{
			    ErrorMessage : 'Ok',				
			    ErrorCode : '200'

			}
